var gulp = require('gulp');
var stylus = require('gulp-stylus');

gulp.task('stylus', function() {
  return gulp.src('stylus/**/*.styl') // Gets all files ending with .scss in stylus/styl and children dirs
    .pipe(stylus())
    .pipe(gulp.dest('build/css'))
})

gulp.watch('stylus/**/*.styl', ['stylus']);

gulp.task('default', ['stylus']);